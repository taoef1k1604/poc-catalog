package org.taufik.poc.catalog.orm;

import org.taufik.poc.catalog.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
